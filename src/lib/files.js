import fs from 'fs'
import path from 'path'

module.exports = {
  getCurrentDirectoryBase() {
    return path.basename(process.cwd())
  },

  directoryExists(filePath) {
    try {
      return fs.statSync(filePath).isDirectory()
    } catch (err) {
      return false
    }
  },
}
